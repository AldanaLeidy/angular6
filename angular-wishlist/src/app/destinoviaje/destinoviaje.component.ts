import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../models/destinoViaje.model';

@Component({
  selector: 'app-destinoviaje',
  templateUrl: './destinoviaje.component.html',
  styleUrls: ['./destinoviaje.component.css']
})
export class DestinoviajeComponent implements OnInit {
  @Input() destino: DestinoViaje;

  @HostBinding('attr.class') cssClass = 'col-md-4';

  constructor() { 
    this.destino=new DestinoViaje('', '');
  }

  ngOnInit(): void {
  }

}
